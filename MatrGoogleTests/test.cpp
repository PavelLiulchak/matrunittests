#include "pch.h"
#include "../../Matrices/Matrices/MATR.h"

TEST(Matr33_Methods_Tests, CFieldAccessTest) {

	Matr33 A;
	double test_value = 258.5;
	A.C(0, 0) = test_value;

	ASSERT_EQ(A.C(0, 0), test_value);
	EXPECT_TRUE(true);
}

TEST(Matr33_Methods_Tests, BracketFieldAccessTest) {

	Matr33 A;
	double test_value = 258.5;
	A(0, 0) = test_value;

	ASSERT_EQ(A(0, 0), test_value);
	EXPECT_TRUE(true);
}

TEST(Matr33_Methods_Tests, Tonull) {

	Matr33 A;
	double test_value = 258.5;
	
	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
		{
			A.C(i, j) = test_value;
		}

	A.tonull();

	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
		{
			EXPECT_EQ(A.C(i, j), 0.0);
		}

	EXPECT_TRUE(true);
}

TEST(Matr33_Constr_Tests, DefaultConstrTest) {

    Matr33 A;	
	for (int i=0;i<3;i++)
		for (int j = 0; j < 3; j++)
		{
			EXPECT_EQ(A.C(i,j), DBL_MAX);
		}
	EXPECT_TRUE(true);
}

TEST(Matr33_Operators_Tests, AssignmentOpeator) {

	Matr33 A;
	Matr33 B;
	double test_value = 258.5;

//CASE### 1 Matr33& operator=(const Matr33& M2);

	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
		{
			A.C(i, j)= test_value;
		}

	B = A;

	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
		{
			EXPECT_EQ(B.C(i, j), A.C(i,j))<<"SEE CASE### 1";
		}

//CASE### 2 Matr33& operator=(const double& d);

	test_value = 2.2258;

	A = test_value;

	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
		{
			EXPECT_EQ(A.C(i, j), test_value) << "SEE CASE### 2";
		}

	EXPECT_TRUE(true);
}

TEST(Matr33_Operators_Tests, Add_AssgnOperator) {

	Matr33 A;
	Matr33 B;

//CASE### 1
	double test_value1 = 0.0;
	double test_value2 = 145.589;

	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
		{
			A.C(i, j) = test_value1;
			B.C(i, j) = test_value2;
		}

	A += B;

	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
		{
			EXPECT_EQ(A.C(i, j), B.C(i, j))<< "SEE CASE### 1";
		}

//CASE### 2
	test_value1 = -145.589;
	test_value2 = 145.589;

	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
		{
			A.C(i, j) = test_value1;
			B.C(i, j) = test_value2;
		}

	A += B;

	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
		{
			EXPECT_EQ(A.C(i, j), 0.0) << "SEE CASE### 2";
		}

	EXPECT_TRUE(true);
}

TEST(Matr33_Operators_Tests, Sub_AssgnOperator) {

	Matr33 A;
	Matr33 B;

	//CASE### 1
	double test_value1 = 0.0;
	double test_value2 = 145.589;

	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
		{
			A.C(i, j) = test_value1;
			B.C(i, j) = test_value2;
		}

	A -= B;

	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
		{
			EXPECT_EQ(A.C(i, j), -test_value2) << "SEE CASE### 1";
		}

	//CASE### 2
	test_value1 = 145.589;
	test_value2 = 145.589;

	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
		{
			A.C(i, j) = test_value1;
			B.C(i, j) = test_value2;
		}

	A -= B;

	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
		{
			EXPECT_EQ(A.C(i, j), 0.0) << "SEE CASE### 2";
		}

	EXPECT_TRUE(true);
}


TEST(Matr33_Operators_Tests, Mult_AssgnOperator) {

	Matr33 A;
	Matr33 B;

	//CASE### 1.1	Matr33& operator*=(const Matr33& M2);
	double test_value1 = 1.0;
	double test_value2 = 1.0;
	
	double test_value_res = 0.0;
	for (int i = 0; i < 3; i++)
		test_value_res += test_value1 * test_value1; //test_value_res has to be equal 3

	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
		{
			A.C(i, j) = test_value1;
			B.C(i, j) = test_value2;
		}

	A *= B;

	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
		{
			EXPECT_EQ(A.C(i, j), test_value_res) << "SEE CASE### 1.1";
		}
	
	

	//CASE### 1.2	 Matr33& operator*=(const Matr33& M2);
	A.tonull();
	B.tonull();

	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
		{
			if (i == j) { A.C(i, j) = 1.0; B.C(i,j) = 1.0; }	//Identity matrices
		}

	A *= B;

	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
		{
			EXPECT_EQ(A.C(i, j), B.C(i, j)) << "SEE CASE### 1.2";
		}

	//CASE### 2.1 Matr33& operator*=(const double& d);
	A.tonull();

	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
		{
			A.C(i, j) = 1.0;
		}

	test_value1 = 25.256;
	A *= test_value1;

	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
		{
			EXPECT_EQ(A.C(i, j), test_value1) << "SEE CASE### 2.1";
		}

	//CASE### 2.2 Matr33& operator*=(const double& d);
	A.tonull();

	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
		{
			A.C(i, j) = 1.0;
		}

	test_value1 = 0.0;
	A *= test_value1;

	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
		{
			EXPECT_EQ(A.C(i, j), test_value1) << "SEE CASE### 2.2";
		}

	EXPECT_TRUE(true);
}

TEST(Matr33_Operators_Tests, Div_AssgnOperator) {

	Matr33 A;
	double test_value1 = 27.0;
	double test_divider = -3.0;
	double test_result = -9.0;
	//CASE### 1.1 Matr33& operator/=(const double& d);
	A.tonull();

	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
		{
			A.C(i, j) = test_value1;
		}

	A /= test_divider;

	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
		{
			EXPECT_EQ(A.C(i, j), test_result) << "SEE CASE### 1.1";
		}

	test_value1 = 25.0;
	test_divider = 5.0;
	test_result = 5.0;
	//CASE### 1.2 Matr33& operator/=(const double& d);
	A.tonull();

	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
		{
			A.C(i, j) = test_value1;
		}

	A /= test_divider;

	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
		{
			EXPECT_EQ(A.C(i, j), test_result) << "SEE CASE### 1.2";
		}

	EXPECT_TRUE(true);
}